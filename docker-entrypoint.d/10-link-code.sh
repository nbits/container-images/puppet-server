#!/bin/sh

set -ex

CODE_DIR=/etc/puppet/code/environments/production
mkdir -p ${CODE_DIR}
if [ -d "${CI_PROJECT_DIR}" ]; then
        rm -rf ${CODE_DIR}/modules
        ln -sf ${CI_PROJECT_DIR} ${CODE_DIR}/modules
fi
