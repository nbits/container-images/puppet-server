FROM debian:bullseye

HEALTHCHECK --interval=10s --timeout=15s --retries=12 --start-period=3m CMD ["/usr/bin/curl", "-k", "-I", "https://127.0.0.1:8140"]

RUN apt-get update -qq && apt install -y \
	augeas-tools \
	curl \
	git \
	puppet-master \
	ruby-scanf
RUN puppet config set autosign "true" --section master

COPY entrypoint.sh /
COPY docker-entrypoint.d /docker-entrypoint.d

ENTRYPOINT /entrypoint.sh
